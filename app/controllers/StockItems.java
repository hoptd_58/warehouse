package controllers;

import play.mvc.*;
import models.StockItem;
import java.util.List;


public class StockItems extends Controller {
    public static Result index() {
        List<StockItem> items = StockItem.find
                .where()
                .ge("quantity", 300)
                .orderBy("quantity desc")
                .setMaxRows(5)
                .findList();
        return ok(items.toString());
    }
}