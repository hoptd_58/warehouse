package models;

import play.data.format.Formats;
import play.mvc.PathBindable;
import javax.persistence.*;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import play.data.validation.Constraints;
import com.avaje.ebean.Page;
import play.db.ebean.Model;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;



@Entity
public class Product extends Model implements PathBindable<Product> {
//public class Product extends Model {
    @Id
    public Long id;
    @Constraints.Required
    public String ean;
    @Constraints.Required
    public String name;
    public String description;
    @OneToMany(mappedBy="product")
    public List<StockItem> stockItems;
    public byte[] picture;
    @ManyToMany
    public List<Tag> tags;
//    public List<Tag> tags = new LinkedList<Tag>();

    @Formats.DateTime(pattern = "yyyy-MM-dd")
    public Date date;

    public static Finder<Long,Product> find = new Finder<Long,Product>(
            Long.class, Product.class
    );

    public Product() {}
    public Product(String ean, String name, String description) {
        this.ean = ean;
        this.name = name;
        this.description = description;
    }
    public String toString() {
        return String.format("%s - %s", ean, name);
    }



    private static List<Product> products;
    static {
        products = new ArrayList<Product>();
        products.add(new Product("1111111111111", "Paperclips 1",
                "Paperclips description 1"));
        products.add(new Product("2222222222222", "Paperclips 2",
                "Paperclips description 2"));
        products.add(new Product("3333333333333", "Paperclips 3",
                "Paperclips description 3"));
        products.add(new Product("4444444444444", "Paperclips 4",
                "Paperclips description 4"));
        products.add(new Product("5555555555555", "Paperclips 5",
                "Paperclips description 5"));
    }

    public void delete() {
        for (Tag tag : tags) {
            tag.products.remove(this);
            tag.save();
        }
        super.delete();
    }

    public static List<Product> findAll() {
        return find.all();
    }


    public static Product findByEan(String ean) {
        return find.where().eq("ean", ean).findUnique();
    }

    public static List<Product> findByName(String term) {
        final List<Product> results = new ArrayList<Product>();
        for (Product candidate : products) {
            if (candidate.name.toLowerCase().contains(term.toLowerCase())) {
                results.add(candidate);
            }
        }
        return results;
    }


    public static Page<Product> find(int page) {
        return find.where()
                .orderBy("id asc")
                .findPagingList(5)
                .setFetchAhead(false)
                .getPage(page);
    }

    @Override
    public Product bind(String key, String value) {
        return findByEan(value);
    }

    @Override
    public String unbind(String key) {
        return ean;
    }

    @Override
    public String javascriptUnbind() {
        return ean;
    }
}